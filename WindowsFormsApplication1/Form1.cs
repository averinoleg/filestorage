﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.ServiceReference1;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        const int PortionSize = 10;

        public Form1()
        {
            InitializeComponent();

            FileStrorageClient client = new FileStrorageClient();

            FilesGrid.AutoGenerateColumns = false;
            FilesGrid.DataSource = client.GetFilesList();

            client.Close();
        }

        private void buttonGetFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                String path = dialog.FileName;
                byte[] data = File.ReadAllBytes(path);
                FileStrorageClient client = new FileStrorageClient();

                byte[] hash;

                using (MD5 md5Hash = MD5.Create())
                    hash = md5Hash.ComputeHash(data);

                if (client.GetFilesList().Count(a => a.HashCode.SequenceEqual(hash)) == 0)
                {
                    FileDescription desc = new FileDescription();

                    desc.Id = Guid.NewGuid();
                    desc.Name = Path.GetFileName(path);
                    desc.Size = data.Length;
                    desc.HashCode = hash;

                    if (!client.CreateFile(desc))
                    {
                        MessageBox.Show("Failed to create file", "Error", MessageBoxButtons.OK);
                        return;
                    }

                    FilesGrid.DataSource = client.GetFilesList();

                    for (int i = 0; i < data.Length; i += PortionSize)
                    {
                        if (!client.UploadData(desc.Id, i, data.Skip(i).Take(PortionSize).ToArray()))
                        {
                            MessageBox.Show("Failed to upload data", "Error", MessageBoxButtons.OK);
                            return;
                        }

                        FilesGrid.DataSource = client.GetFilesList();
                    }
                }
                else
                {
                    FileDescription desc = client.GetFilesList().First(a => a.HashCode.SequenceEqual(hash));

                    for (int i = desc.BytesLoaded; i < data.Length; i += PortionSize)
                    {
                        if (!client.UploadData(desc.Id, i, data.Skip(i).Take(PortionSize).ToArray()))
                        {
                            MessageBox.Show("Failed to upload data", "Error", MessageBoxButtons.OK);
                            return;
                        }

                        FilesGrid.DataSource = client.GetFilesList();
                    }
                }

                client.Close();
            }
        }

        private void FilesGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                Stream stream;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.FileName = FilesGrid.Rows[e.RowIndex].Cells["Name"].Value.ToString();

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if ((stream = saveFileDialog.OpenFile()) != null)
                    {
                        FileStrorageClient client = new FileStrorageClient();

                        var data = client.DownLoad(Guid.Parse(FilesGrid.Rows[e.RowIndex].Cells["Id"].Value.ToString()));
                        client.Close();

                        stream.Write(data, 0, data.Length);
                        stream.Close();
                    }
                }
            }
        }
    }
}
