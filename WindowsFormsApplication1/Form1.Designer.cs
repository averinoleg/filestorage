﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGetFile = new System.Windows.Forms.Button();
            this.FilesGrid = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BytesLoaded = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Download = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Download.Text = "Загрузить";
            ((System.ComponentModel.ISupportInitialize)(this.FilesGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonGetFile
            // 
            this.buttonGetFile.Location = new System.Drawing.Point(13, 13);
            this.buttonGetFile.Name = "buttonGetFile";
            this.buttonGetFile.Size = new System.Drawing.Size(187, 23);
            this.buttonGetFile.TabIndex = 0;
            this.buttonGetFile.Text = "Загрузить файл";
            this.buttonGetFile.UseVisualStyleBackColor = true;
            this.buttonGetFile.Click += new System.EventHandler(this.buttonGetFile_Click);
            // 
            // FilesGrid
            // 
            this.FilesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FilesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Name,
            this.Size,
            this.BytesLoaded,
            this.Download});
            this.FilesGrid.Location = new System.Drawing.Point(13, 43);
            this.FilesGrid.Name = "FilesGrid";
            this.FilesGrid.Size = new System.Drawing.Size(696, 229);
            this.FilesGrid.TabIndex = 1;
            this.FilesGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.FilesGrid_CellContentClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "Id";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Name
            // 
            this.Name.DataPropertyName = "Name";
            this.Name.HeaderText = "Название";
            this.Name.Name = "Name";
            this.Name.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.DataPropertyName = "Size";
            this.Size.HeaderText = "Размер";
            this.Size.Name = "Size";
            this.Size.ReadOnly = true;
            // 
            // BytesLoaded
            // 
            this.BytesLoaded.DataPropertyName = "BytesLoaded";
            this.BytesLoaded.HeaderText = "Загружено";
            this.BytesLoaded.Name = "BytesLoaded";
            this.BytesLoaded.ReadOnly = true;
            // 
            // Download
            // 
            this.Download.HeaderText = "Загрузить";
            this.Download.Text = "Загрузить";
            this.Download.UseColumnTextForButtonValue = true;
            this.Download.Name = "Download";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 284);
            this.Controls.Add(this.FilesGrid);
            this.Controls.Add(this.buttonGetFile);
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.FilesGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonGetFile;
        private System.Windows.Forms.DataGridView FilesGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn BytesLoaded;
        private System.Windows.Forms.DataGridViewButtonColumn Download;
    }
}

