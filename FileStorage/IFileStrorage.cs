﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FileStorage
{
    [ServiceContract]
    public interface IFileStrorage
    {

        [OperationContract]
        bool CreateFile(FileDescription desc);
        [OperationContract]
        bool UploadData(Guid id, int start, byte[] data);
        [OperationContract]
        byte[] DownLoad(Guid Id);
        [OperationContract]
        List<FileDescription> GetFilesList();
    }

    [Serializable]
    [DataContract]
    public class FileDescription
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public byte[] HashCode { get; set; }
        [DataMember]
        public int Size { get; set; }
        [DataMember]
        public int BytesLoaded { get; set; }
        public int Percent
        {
            get
            {
                return BytesLoaded / Size * 100;
            }
        }
    }
}
