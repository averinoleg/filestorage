﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FileStorage
{
    public class FileStrorage : IFileStrorage
    {
        const string StorageFolder = @"D:\FileStorage\";
        const string FilesFolder = StorageFolder + @"Files\";
        const string DBFile = StorageFolder + @"DBFile.xml";

        volatile static Object s_SyncObject;

        static List<FileDescription> s_FilesDescrioption;

        static FileStrorage()
        {
            AppDomain.CurrentDomain.ProcessExit += StaticClass_Dtor;

            if (File.Exists(DBFile))
            {
                FileStream stream = File.OpenRead(DBFile);
                var formatter = new BinaryFormatter();
                s_FilesDescrioption = (List<FileDescription>)formatter.Deserialize(stream);
                stream.Close();
            }
            else
            {
                s_FilesDescrioption = new List<FileDescription>();
            }
        }

        static void StaticClass_Dtor(object sender, EventArgs e)
        {
            SaveDescriptions();
        }

        static void SaveDescriptions()
        {
            FileStream stream = File.Create(DBFile);
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, s_FilesDescrioption);
            stream.Close();
        }

        bool IFileStrorage.CreateFile(FileDescription desc)
        {
            if (s_FilesDescrioption.Any(a => a.HashCode.SequenceEqual(desc.HashCode)))
                return false;

            var stream = File.Create(FilesFolder + desc.Id);
            stream.Close();

            s_FilesDescrioption.Add(desc);

            SaveDescriptions();

            return true;
        }

        bool IFileStrorage.UploadData(Guid id, int start, byte[] data)
        {
            try
            {
                if (s_FilesDescrioption.All(a => a.Id != id))
                    return false;

                FileDescription desc = s_FilesDescrioption.First(a => a.Id == id);

                if (desc.BytesLoaded != start)
                    return false;

                using (var stream = new FileStream(FilesFolder + desc.Id, FileMode.Append))
                {
                    stream.Write(data, 0, data.Length);
                    desc.BytesLoaded += data.Length;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        byte[] IFileStrorage.DownLoad(Guid Id)
        {
            if (!s_FilesDescrioption.Any(a => a.Id == Id))
                return null;

            try
            {
                System.IO.FileStream stream = System.IO.File.Open(FilesFolder + Id.ToString(), FileMode.Open, FileAccess.Read);
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, (int)stream.Length);
                stream.Close();
                return data;
            }
            catch
            {
                return null;
            }
        }

        List<FileDescription> IFileStrorage.GetFilesList()
        {
            return s_FilesDescrioption;
        }
    }
}
